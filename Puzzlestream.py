#!/bin/python3

# -*- coding: utf-8 -*-
import re
import sys
import puzzlestream.launch as launch

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    if "--nolog" not in sys.argv:
        launch.setStdout()
    else:
        del sys.argv[sys.argv.index("--nolog")]
    if "--viewer" in sys.argv:
        pathPos = sys.argv.index("--viewer")+1
        if len(sys.argv) >= pathPos+1:
            sys.exit(launch.launchPSViewer(path=sys.argv[pathPos]))
        else:
            sys.exit(launch.launchPSViewer())
    else:
        sys.exit(launch.launchPuzzlestream())
