.. Puzzlestream documentation master file, created by
   sphinx-quickstart on Wed Oct 17 10:51:14 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Puzzlestream
============

Puzzlestream is an interactive analysis enviroment for Python, providing a
fast and simple way from raw data to meaningful results and visualisations.
By organising your code in modules, Puzzlestream gives you an instantaneous
overview of your project's structure - however complicated it may be. Highly
interactive graphical interfaces support you in gaining an intuition for your
data, asking the right questions and finding the corresponding answers.
Platform independence and easy installation allow a quick start on any system
you like.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   index-user
   troubleshooting
   documentation
   releaseinfo


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
