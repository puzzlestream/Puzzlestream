Installation
============

Windows
-------

The recommended way to install Puzzlestream is a simple Window installer you
can find here_.

.. _here: http://documentation.puzzlestream.org/Puzzlestream-inst.exe

Simply download the file, run the installer and you are ready to go.
The installation already includes packages such as numpy, scipy and matplotlib;
any PyPi (Python pakage index) package can be added after the installation.
Packages that have already been installed beforehand (using a different Python
installation) will be available in Puzzlestream.

If you are already using Python and know what you are doing, you may install
Puzzlestream as a pip-package to save a little storage space. Check your
Python version first by running::

    python -V

in the command line; at least, Python 3.5 is required. We recommend getting
the newest Python version at https://www.python.org/downloads/windows/.
Puzzlestream can then be installed using::

    python -m pip install --upgrade puzzlestream

Linux
-----

On Linux, Puzzlestream can be installed in four easy steps. If you know that
pip for Python ≥ 3.5 is already installed on your system, you can directly
jump to step 4.

1. Open a terminal (Ubuntu: Ctrl + Alt + T).
2. Check your current Python version.
   Run::

       python3 -V

   in the command line. Python 3 should be installed on all modern Linux distributions,
   at least Python 3.5 is required.
3. Install pip for Python 3.
   For Debian-based Linux distributions (Ubuntu, Debian, Linux Mint, ...), run::

       sudo apt-get install python3-pip​

   On Red Hat-based LInux distributions (Fedora, Cent OS, Scientific Linux, RHEL, ...), run::

       sudo dnf install python3-pip

4. Install Puzzlestream using pip.
   Run::

       python3 -m pip install --upgrade --user puzzlestream

   in the command line to install Puzzlestream.


Mac OS X
--------
