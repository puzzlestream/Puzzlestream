puzzlestream.apps package
=========================

.. automodule:: puzzlestream.apps
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    puzzlestream.apps.plot

Submodules
----------

.. toctree::

   puzzlestream.apps.base

