puzzlestream.ui package
=======================

.. automodule:: puzzlestream.ui
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

.. toctree::

   puzzlestream.ui.about
   puzzlestream.ui.codeeditor
   puzzlestream.ui.codestyle
   puzzlestream.ui.colors
   puzzlestream.ui.dataview
   puzzlestream.ui.dataviewexport
   puzzlestream.ui.dock
   puzzlestream.ui.editorwidget
   puzzlestream.ui.fileexplorer
   puzzlestream.ui.gittab
   puzzlestream.ui.graphicsscene
   puzzlestream.ui.graphicsview
   puzzlestream.ui.main
   puzzlestream.ui.manager
   puzzlestream.ui.module
   puzzlestream.ui.moduleheader
   puzzlestream.ui.modulestatusbar
   puzzlestream.ui.modulewidget
   puzzlestream.ui.nameedit
   puzzlestream.ui.notification
   puzzlestream.ui.notificationtab
   puzzlestream.ui.outputtextedit
   puzzlestream.ui.pip
   puzzlestream.ui.pipe
   puzzlestream.ui.plot
   puzzlestream.ui.plotTest
   puzzlestream.ui.plotview
   puzzlestream.ui.preferences
   puzzlestream.ui.preferencestranslation
   puzzlestream.ui.puzzledockitem
   puzzlestream.ui.puzzleitem
   puzzlestream.ui.resources
   puzzlestream.ui.switch
   puzzlestream.ui.tableplot
   puzzlestream.ui.tracebackview
   puzzlestream.ui.translate
   puzzlestream.ui.valve

