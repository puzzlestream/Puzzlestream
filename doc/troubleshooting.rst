Troubleshooting
===============

If you experience any problems, please either open an issue on https://git.rwth-aachen.de/puzzlestream/Puzzlestream/issues or contact us using the form located here_.

.. _here: https://puzzlestream.org/support-german/kontakt

You may generate an aggregation of the debug information using the menu entry
Help -> Save debug information.
Please provide the generated zip-file in case of an error or crash!
