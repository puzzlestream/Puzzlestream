Your first project
==================

To create your first project, start Puzzlestream and create a new project
folder:

.. image:: first-media/newProject.gif

You may now create a first module by clicking on "New module" and add the
module to the puzzle by clicking on an arbitrary position in the puzzle area:

.. image:: first-media/newModule.gif

Now, click on the module you just created to show its code on the left side of
the main window.
Its only ingredient is the function "main" that has only one input, the
"stream". The stream behaves like a usual dictionary and has to be returned by
the "main" function. It will then be passed through pipes and valves to the
next module. The module code can be executed by either clicking on the "play"
button on the module itself or the play button in the upper left corner of the
editor:

.. image:: first-media/runModule.gif

The Puzzlestream main window
============================

In its initial state, the main window consists of the puzzle area, an editor,
and a tab view, as shown in the sketch below.

.. image:: first-media/mainWindow.png

The basic working principle is a "stream" of data flowing through a system of
modules, pipes, and valves (called the "puzzle"). Modules may load, edit, and
save items from or to the stream, pipes and valves are used for connecting
modules and giving a structure to the puzzle. A typical structure could be
read module -> analyse module -> export/plot module. The great advantages of
this approach compared to standard Python scripts are

- the inherent structure of the project and
- there is no need for full project runs - if you only want to change a label
  in a plot, you just need to run the plot module and not the complete
  (possibly very time consuming) analysis.

Adding pipes and valves to the puzzle
=====================================

You may now rename the module you created above:

.. image:: first-media/renameModule.gif

This module may now be connected to a second module using a pipe by clicking
on "New pipe":

.. image:: first-media/secondModule.gif

You may also add a valve in between:

.. image:: first-media/newPipeValve.gif

It gives you the opportunity to split the flow of the "stream" into multiple
paths, each leading to different modules.
