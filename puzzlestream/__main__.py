if __name__ == "__main__":
    from puzzlestream.launch import launchPuzzlestream, setStdout
    import sys

    setStdout()
    sys.exit(launchPuzzlestream())
