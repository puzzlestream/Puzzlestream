# -*- coding: utf-8 -*-
from matplotlib.figure import Figure
from matplotlib.axis import Axis
import numpy as np
import pandas as pd
from puzzlestream.apps.base import PSApp, PSAppGUIWidget
from puzzlestream.ui.plot import Plot
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

app = "PSLinearRegressionApp"
name = "Linear regression"
# icon = "icon.png"


class PSLinearRegressionApp(PSApp):

    def __init__(self, data: dict, **pars):
        super().__init__(data, **pars)
        self._guiWidgetClass = PSLinearRegressionAppGUI

    @property
    def code(self) -> str:
        return super().code

    def setParameters(self, **pars):
        super().setParameters(**pars)


class PSLinearRegressionAppGUI(PSAppGUIWidget):

    def __init__(self, app: PSApp, parent=None):
        super().__init__(app, parent=parent)
        self.__app = app
        self.__layout = QGridLayout()
        self.setLayout(self.__layout)

    def reload(self):
        pass

    def updatePlot(self):
        pass

    def retranslateUi(self):
        pass
