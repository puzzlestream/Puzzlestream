# -*- coding: utf-8 -*-
"""Stream module.

contains PSStream
"""

import os
import re
from time import sleep

import h5py
import pickle
import numpy as np

from puzzlestream.backend.reference import PSCacheReference


class PSStream:

    """Stream class bla."""

    def __init__(self, path: str):
        """Cache init.

        Args:
            path (str): Cache file.
        """
        self.__path = path
        if not os.path.isdir(self.__path):
            os.mkdir(self.__path)

    @property
    def references(self):
        ref = []
        for key in self:
            ID = key.split("-")[0]
            keyn = key[len(ID) + 1:]
            if self.__checkIfReference(ID, keyn):
                ref.append(key)
        return ref

    def __openFile(self, path: str, mode: str = "r"):
        n_attempts = 0
        while n_attempts < 20:
            try:
                f = h5py.File(path, mode)
                break
            except OSError:
                n_attempts += 1
                sleep(0.25)
        else:
            raise OSError("Could not open stream file, file is locked.")

        return f

    def __getPath(self, ID: int, key: str) -> str:
        return os.path.join(self.__path, str(ID), key + ".hdf5")

    def __checkIfReference(self, ID: int, key: str):
        f = self.__openFile(self.__getPath(ID, key), "r")
        if "isReference" in f:
            isReference = f["isReference"][()]
        else:
            isReference = False
        f.close()
        return isReference

    def __checkIfOutdatedReference(self, ID: int, key: str) -> bool:
        if not self.__checkIfReference(ID, key):
            return False

        ref = self.__readData(ID, key)
        if os.path.isfile(self.__getPath(str(ref.sectionID), key)):
            return False
        return True

    def __readData(self, ID: int, key: str) -> object:
        f = self.__openFile(self.__getPath(ID, key), "r")
        if "data" in f:
            data = f["data"][()]
        else:
            data = None

        if "isPickled" in f and f["isPickled"][()]:
            data = pickle.loads(data)
        f.close()

        if self.__checkIfReference(ID, key):
            data = PSCacheReference(int(re.findall(r"\d+", data)[0]))
        return data

    def __writeData(self, ID: int, key: str, data: object):
        if not os.path.isdir(os.path.join(self.__path, str(ID))):
            os.mkdir(os.path.join(self.__path, str(ID)))

        f = self.__openFile(self.__getPath(ID, key), "w")
        kwargs = {}
        if isinstance(data, np.ndarray):
            f["isPickled"] = False
            kwargs["compression"] = "gzip"
            kwargs["compression_opts"] = 6
        elif (isinstance(data, int) or isinstance(data, float) or
              isinstance(data, str) or isinstance(data, complex) or
              isinstance(data, bool)):
            f["isPickled"] = False
        else:
            f["isPickled"] = True
            data = pickle.dumps(data, 0)

        kwargs["data"] = data
        f.create_dataset("data", **kwargs)

        if (isinstance(data, str) and data.startswith("!!!ref-")
                and data.endswith("!!!")):
            f["isReference"] = True
        else:
            f["isReference"] = False
        f.close()

    def __contains__(self, key: str) -> bool:
        ID = key.split("-")[0]
        keyn = key[len(ID) + 1:]
        path = self.__getPath(ID, keyn)
        if os.path.isfile(path):
            if not self.__checkIfOutdatedReference(int(ID), keyn):
                return True
        return False

    def __iter__(self):
        for folder in os.listdir(self.__path):
            for f in os.listdir(os.path.join(self.__path, folder)):
                if f.endswith(".hdf5"):
                    yield folder + "-" + os.path.splitext(f)[0]

    def deleteItem(self, ID: int, key: str):
        try:
            os.remove(self.__getPath(ID, key))
        except Exception as e:
            f.close()
            raise e

    def __getitem__(self, key: str) -> object:
        """Legacy function, should be removed in the future."""
        ID = key.split("-")[0]
        keyn = key[len(ID) + 1:]
        return self.getItem(ID, key)

    def getItem(self, ID: int, key: str) -> object:
        """Get item from cache.

        Args:
            ID (int): Stream section ID.
            key (str): Requested key.

        Returns:
            Cached data at "ID-key".
        """
        if self.__checkIfOutdatedReference(ID, key):
            raise KeyError(key + " not found for item %d." % (ID))

        return self.__readData(ID, key)

    def setItem(self, ID: int, key: str, data: object):
        """Store data in cache.

        Args:
            ID (int): Stream section ID.
            key (str): Key to use.
            data: Data that should be stored; must be pickable!
        """
        if isinstance(data, PSCacheReference):
            data = "!!!ref-" + str(data) + "!!!"
        if data is None:
            data = str(data)
        self.__writeData(ID, key, data)
