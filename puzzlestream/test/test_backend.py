from os import path

from puzzlestream.backend import notificationsystem
from puzzlestream.backend.config import PSConfig


def test_config():
    conf = PSConfig()
    conf.edited.connect(print)
    conf["numberOfProcesses"] = 0
    assert conf["numberOfProcesses"] == 0
    conf.addRecentProject(path.abspath("."))
    assert len(conf["last projects"]) > 0
    conf.save()


def test_notificationsystem():
    reacted = False

    def react(message):
        assert message == "Test"
        nonlocal reacted
        reacted = True

    notificationsystem.addReactionMethod(react)
    assert len(notificationsystem.reactionMethods) == 1
    notificationsystem.newNotification("Test")
    assert reacted
    notificationsystem.removeReactionMethod(react)
    assert len(notificationsystem.reactionMethods) == 0
