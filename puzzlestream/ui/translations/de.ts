<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>About</name>
    <message>
        <location filename="../about.py" line="35"/>
        <source>&lt;p&gt;Puzzlestream is an interactive analysis enviroment for Python, providing a fast and simple way from raw data to meaningful results and visualisations. By organising your code in modules, Puzzlestream gives you an instantaneous overview of your project&apos;s structure - however complicated it may be. Highly interactive graphical interfaces support you in gaining an intuition for your data, asking the right questions and finding the corresponding answers. Platform independence and easy installation allow a quick start on any system you like.&lt;/p&gt;&lt;p&gt;More information and the documentation can be found &lt;a href=&quot;https://puzzlestream.org&quot;&gt;here&lt;/a&gt;.</source>
        <translation type="obsolete">&lt;p&gt;Puzzlestream ist eine interaktive Analyseumgebung für Python, die einen schnellen und einfachen Weg von Rohdaten zu aussagekräftigen Ergebnissen und Visualisierungen bietet. Durch die modulare Organisation Ihres Codes gibt Ihnen Puzzlestream einen sofortigen Überblick über die Struktur Ihres Projekts - so kompliziert es auch sein mag. Hochinteraktive grafische Oberflächen unterstützen Sie dabei, eine Intuition für Ihre Daten zu gewinnen, die richtigen Fragen zu stellen und die entsprechenden Antworten zu geben. Plattformunabhängigkeit und einfache Installation ermöglichen einen schnellen Start auf jedem beliebigen System.&lt;/p&gt;&lt;p&gt;Mehr Informationen und die Dokumentation können &lt;a href=&quot;https://puzzlestream.org&quot;&gt;hier&lt;/a&gt; gefunden werden.</translation>
    </message>
    <message>
        <location filename="../about.py" line="39"/>
        <source>&lt;p&gt;Puzzlestream is an interactive analysis enviroment for Python, providing a fast and simple way from raw data to meaningful results and visualisations. By organising your code in modules, Puzzlestream gives you an instantaneous overview of your project&apos;s structure - however complicated it may be. Highly interactive graphical interfaces support you in gaining an intuition for your data, asking the right questions and finding the corresponding answers. Platform independence and easy installation allow a quick start on any system you like.&lt;/p&gt;&lt;p&gt;More information and the documentation can be found &lt;a href=&quot;https://puzzlestream.org&quot;&gt;here&lt;/a&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Puzzlestream ist eine interaktive Analyseumgebung für Python, die einen schnellen und einfachen Weg von Rohdaten zu aussagekräftigen Ergebnissen und Visualisierungen bietet. Durch die modulare Organisation Ihres Codes gibt Ihnen Puzzlestream einen sofortigen Überblick über die Struktur Ihres Projekts - so kompliziert es auch sein mag. Hochinteraktive grafische Oberflächen unterstützen Sie dabei, eine Intuition für Ihre Daten zu gewinnen, die richtigen Fragen zu stellen und die entsprechenden Antworten zu geben. Plattformunabhängigkeit und einfache Installation ermöglichen einen schnellen Start auf jedem beliebigen System.&lt;/p&gt;&lt;p&gt;Mehr Informationen und die Dokumentation können &lt;a href=&quot;https://puzzlestream.org&quot;&gt;hier&lt;/a&gt; gefunden werden.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>DataView</name>
    <message>
        <location filename="../dataview.py" line="186"/>
        <source>Export complete table</source>
        <translation>Komplette Tabelle exportieren</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="181"/>
        <source>Data view</source>
        <translation>Datenanzeige</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="176"/>
        <source>complete stream</source>
        <translation>kompletter Strom</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="188"/>
        <source>swap axes</source>
        <translation>Achsen tauschen</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="592"/>
        <source>Confirm clean up</source>
        <translation>Aufräumen bestätigen</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="592"/>
        <source>Are you sure you want to erase</source>
        <translation>Sind Sie sicher, dass Sie</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="592"/>
        <source>from the stream?</source>
        <translation>aus dem Strom löschen möchten?</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="670"/>
        <source>Set as x</source>
        <translation type="obsolete">Als x setzen</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="672"/>
        <source>Set as y</source>
        <translation type="obsolete">Als y setzen</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="688"/>
        <source>Show traceback</source>
        <translation>Rückverfolgung anzeigen</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="691"/>
        <source>Delete from stream</source>
        <translation>Aus dem Strom löschen</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="699"/>
        <source>Calculate mean, sum and std.</source>
        <translation>Mittelwert, Summe und Stdabw. berechnen</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="702"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="797"/>
        <source>Sum</source>
        <translation>Summe</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="798"/>
        <source>Mean</source>
        <translation>Mittelwert</translation>
    </message>
    <message>
        <location filename="../dataview.py" line="802"/>
        <source>Standard deviation</source>
        <translation>Standardabweichung</translation>
    </message>
</context>
<context>
    <name>DataViewExport</name>
    <message>
        <location filename="../dataviewexport.py" line="64"/>
        <source>Please choose an export format</source>
        <translation type="obsolete">Bitte wählen Sie ein Format für den Export</translation>
    </message>
    <message>
        <location filename="../dataviewexport.py" line="72"/>
        <source>Plain text</source>
        <translation>Einfacher Text</translation>
    </message>
    <message>
        <location filename="../dataviewexport.py" line="72"/>
        <source>Comma separated (csv)</source>
        <translation>Komma-separiert</translation>
    </message>
    <message>
        <location filename="../dataviewexport.py" line="72"/>
        <source>Latex</source>
        <translation>Latex</translation>
    </message>
    <message>
        <location filename="../dataviewexport.py" line="81"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../dataviewexport.py" line="82"/>
        <source>Copy to clipboard</source>
        <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
        <location filename="../dataviewexport.py" line="84"/>
        <source>Cancel</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../dataviewexport.py" line="69"/>
        <source>Please choose an export format:</source>
        <translation>Bitte wählen Sie ein Format für den Export aus:</translation>
    </message>
</context>
<context>
    <name>FileExplorer</name>
    <message>
        <location filename="../fileexplorer.py" line="90"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileexplorer.py" line="93"/>
        <source>Cut</source>
        <translation type="unfinished">Ausschneiden</translation>
    </message>
    <message>
        <location filename="../fileexplorer.py" line="96"/>
        <source>Copy</source>
        <translation type="unfinished">Kopieren</translation>
    </message>
    <message>
        <location filename="../fileexplorer.py" line="99"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileexplorer.py" line="102"/>
        <source>Delete</source>
        <translation type="unfinished">Löschen</translation>
    </message>
    <message>
        <location filename="../fileexplorer.py" line="108"/>
        <source>Paste</source>
        <translation type="unfinished">Einfügen</translation>
    </message>
</context>
<context>
    <name>GitTab</name>
    <message>
        <location filename="../gittab.py" line="151"/>
        <source>Changed</source>
        <translation>Geändert</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="303"/>
        <source>Fetch</source>
        <translation>Fetch</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="143"/>
        <source>Stage all changes</source>
        <translation>Alle Änderungen bereitstellen</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="144"/>
        <source>Unstage all changes</source>
        <translation>Alle Bereitstellungen aufheben</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="146"/>
        <source>Revert all changes</source>
        <translation>Alle Änderungen verwerfen</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="147"/>
        <source>Commit</source>
        <translation>Commit</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="126"/>
        <source>Pull</source>
        <translation type="obsolete">Pull</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="127"/>
        <source>Push</source>
        <translation type="obsolete">Push</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="149"/>
        <source>Commit message</source>
        <translation>Commit Nachricht</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="152"/>
        <source>Staged changes</source>
        <translation>Bereitgestellt</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="153"/>
        <source>Stage</source>
        <translation>Bereitstellen</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="154"/>
        <source>Revert</source>
        <translation>Verwerfen</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="155"/>
        <source>Unstage</source>
        <translation>Bereitstellung aufheben</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="148"/>
        <source>Pull and push</source>
        <translation>Pull und Push ausführen</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="305"/>
        <source>Reload</source>
        <translation>Update</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="210"/>
        <source>not available</source>
        <translation>nicht verfügbar</translation>
    </message>
    <message>
        <location filename="../gittab.py" line="448"/>
        <source>Git error:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GraphicsScene</name>
    <message>
        <location filename="../graphicsscene.py" line="445"/>
        <source>Show data</source>
        <translation>Daten anzeigen</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="447"/>
        <source>Show plots</source>
        <translation>Plots anzeigen</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="399"/>
        <source>Change module file path</source>
        <translation>Pfad des Moduls ändern</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="401"/>
        <source>Delete module</source>
        <translation>Modul löschen</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="423"/>
        <source>Close pipe</source>
        <translation>Leitung schließen</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="426"/>
        <source>Open pipe</source>
        <translation>Leitung öffnen</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="429"/>
        <source>Delete pipe</source>
        <translation>Leitung löschen</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="451"/>
        <source>Close valve</source>
        <translation>Verteiler schließen</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="454"/>
        <source>Open valve</source>
        <translation>Verteiler öffnen</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="462"/>
        <source>Delete valve</source>
        <translation>Verteiler löschen</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="488"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../graphicsview.py" line="117"/>
        <source>Are you sure you want to delete</source>
        <translation>Sind Sie sicher, dass Sie</translation>
    </message>
    <message>
        <location filename="../graphicsview.py" line="117"/>
        <source>?</source>
        <translation>löschen möchten?</translation>
    </message>
    <message>
        <location filename="../graphicsview.py" line="122"/>
        <source>Item deletion</source>
        <translation>Objekt löschen</translation>
    </message>
    <message>
        <location filename="../graphicsview.py" line="132"/>
        <source>Are you sure you want to delete the following items?</source>
        <translation>Sind Sie sicher, dass Sie die folgenden Elemente löschen möchten?</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="443"/>
        <source>Run from here</source>
        <translation>Von hier starten</translation>
    </message>
    <message>
        <location filename="../graphicsscene.py" line="457"/>
        <source>Propagate incomplete inputs</source>
        <translation>Unvollständige Eingangssignale weitergeben</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main.py" line="376"/>
        <source>Puzzlestream</source>
        <translation>Puzzlestream</translation>
    </message>
    <message>
        <location filename="../main.py" line="817"/>
        <source>Output</source>
        <translation>Ausgabe</translation>
    </message>
    <message>
        <location filename="../main.py" line="819"/>
        <source>Statistics</source>
        <translation>Statistik</translation>
    </message>
    <message>
        <location filename="../main.py" line="401"/>
        <source>&amp;File</source>
        <translation type="obsolete">&amp;Datei</translation>
    </message>
    <message>
        <location filename="../main.py" line="402"/>
        <source>&amp;Edit</source>
        <translation type="obsolete">&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="../main.py" line="403"/>
        <source>&amp;View</source>
        <translation type="obsolete">&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="../main.py" line="404"/>
        <source>&amp;Libraries</source>
        <translation type="obsolete">Bib&amp;liotheken</translation>
    </message>
    <message>
        <location filename="../main.py" line="405"/>
        <source>&amp;Stream</source>
        <translation type="obsolete">&amp;Strom</translation>
    </message>
    <message>
        <location filename="../main.py" line="406"/>
        <source>&amp;Git</source>
        <translation type="obsolete">&amp;Git</translation>
    </message>
    <message>
        <location filename="../main.py" line="407"/>
        <source>&amp;Help</source>
        <translation type="obsolete">&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../main.py" line="408"/>
        <source>toolBar</source>
        <translation type="obsolete">Toolbar</translation>
    </message>
    <message>
        <location filename="../main.py" line="766"/>
        <source>New module</source>
        <translation>Neues Modul</translation>
    </message>
    <message>
        <location filename="../main.py" line="637"/>
        <source>Close second editor</source>
        <translation>Zweiten Editor schließen</translation>
    </message>
    <message>
        <location filename="../main.py" line="768"/>
        <source>New internal module</source>
        <translation>Internes Modul hinzufügen</translation>
    </message>
    <message>
        <location filename="../main.py" line="770"/>
        <source>New external module</source>
        <translation>Externes Modul hinzufügen</translation>
    </message>
    <message>
        <location filename="../main.py" line="774"/>
        <source>New pipe</source>
        <translation>Neue Leitung</translation>
    </message>
    <message>
        <location filename="../main.py" line="775"/>
        <source>New valve</source>
        <translation>Neuer Verteiler</translation>
    </message>
    <message>
        <location filename="../main.py" line="415"/>
        <source>&amp;New project</source>
        <translation type="obsolete">&amp;Neues Projekt</translation>
    </message>
    <message>
        <location filename="../main.py" line="417"/>
        <source>&amp;Open project</source>
        <translation type="obsolete">Projekt &amp;öffnen</translation>
    </message>
    <message>
        <location filename="../main.py" line="447"/>
        <source>&amp;Save project as...</source>
        <translation>Projekt &amp;speichern unter...</translation>
    </message>
    <message>
        <location filename="../main.py" line="420"/>
        <source>Recent projects</source>
        <translation type="obsolete">Zuletzt verwendete Projekte</translation>
    </message>
    <message>
        <location filename="../main.py" line="426"/>
        <source>&amp;Undo</source>
        <translation type="obsolete">&amp;Rückgängig</translation>
    </message>
    <message>
        <location filename="../main.py" line="427"/>
        <source>&amp;Redo</source>
        <translation type="obsolete">&amp;Wiederherstellen</translation>
    </message>
    <message>
        <location filename="../main.py" line="428"/>
        <source>&amp;Cut</source>
        <translation type="obsolete">&amp;Ausschneiden</translation>
    </message>
    <message>
        <location filename="../main.py" line="429"/>
        <source>&amp;Copy</source>
        <translation type="obsolete">&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="../main.py" line="430"/>
        <source>&amp;Paste</source>
        <translation type="obsolete">&amp;Einfügen</translation>
    </message>
    <message>
        <location filename="../main.py" line="411"/>
        <source>Pre&amp;ferences</source>
        <translation>Ein&amp;stellungen</translation>
    </message>
    <message>
        <location filename="../main.py" line="433"/>
        <source>Open second editor</source>
        <translation>Zweiten Editor öffnen</translation>
    </message>
    <message>
        <location filename="../main.py" line="398"/>
        <source>Show &amp;data</source>
        <translation>&amp;Daten anzeigen</translation>
    </message>
    <message>
        <location filename="../main.py" line="399"/>
        <source>Show &amp;plots</source>
        <translation>&amp;Plots anzeigen</translation>
    </message>
    <message>
        <location filename="../main.py" line="401"/>
        <source>&amp;Clean stream</source>
        <translation>Strom &amp;reinigen</translation>
    </message>
    <message>
        <location filename="../main.py" line="406"/>
        <source>&amp;Fetch / reload</source>
        <translation>&amp;Fetch / neu laden</translation>
    </message>
    <message>
        <location filename="../main.py" line="408"/>
        <source>Pull</source>
        <translation>Pull</translation>
    </message>
    <message>
        <location filename="../main.py" line="409"/>
        <source>Push</source>
        <translation>Push</translation>
    </message>
    <message>
        <location filename="../main.py" line="415"/>
        <source>&amp;About Puzzlestream</source>
        <translation>&amp;Über Puzzlestream</translation>
    </message>
    <message>
        <location filename="../main.py" line="1283"/>
        <source>Notifications</source>
        <translation>Benachrichtigungen</translation>
    </message>
    <message>
        <location filename="../main.py" line="957"/>
        <source>Add lib path</source>
        <translation>Bibliothekpfad hinzufügen</translation>
    </message>
    <message>
        <location filename="../main.py" line="964"/>
        <source>Open folder</source>
        <translation>Ordner öffnen</translation>
    </message>
    <message>
        <location filename="../main.py" line="966"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../main.py" line="443"/>
        <source>Puzzle mode</source>
        <translation type="obsolete">Puzzle</translation>
    </message>
    <message>
        <location filename="../main.py" line="445"/>
        <source>Data view mode</source>
        <translation type="obsolete">Datenanzeige</translation>
    </message>
    <message>
        <location filename="../main.py" line="447"/>
        <source>Plot view mode</source>
        <translation type="obsolete">Plotanzeige</translation>
    </message>
    <message>
        <location filename="../main.py" line="449"/>
        <source>&amp;Toggle fullscreen</source>
        <translation type="obsolete">Vollbild umschalten</translation>
    </message>
    <message>
        <location filename="../main.py" line="1088"/>
        <source>Confirm clean up</source>
        <translation>Aufräumen bestätigen</translation>
    </message>
    <message>
        <location filename="../main.py" line="1088"/>
        <source>Are you sure you want to erase ALL data from the stream?</source>
        <translation>Sind Sie sicher, dass Sie ALLE Daten aus dem Strom löschen möchten?</translation>
    </message>
    <message>
        <location filename="../main.py" line="1327"/>
        <source>New project folder</source>
        <translation>Neuer Projektordner</translation>
    </message>
    <message>
        <location filename="../main.py" line="1403"/>
        <source>Directory not empty.</source>
        <translation>Verzeichnis ist nicht leer.</translation>
    </message>
    <message>
        <location filename="../main.py" line="1348"/>
        <source>Open project folder</source>
        <translation>Projektordner öffnen</translation>
    </message>
    <message>
        <location filename="../main.py" line="1367"/>
        <source>The chosen project folder is not valid. Please choose another one.</source>
        <translation>Der ausgewählte Ordner ist kein gültiger Projektordner. Bitte wählen Sie ein anderes Verzeichnis.</translation>
    </message>
    <message>
        <location filename="../main.py" line="1422"/>
        <source>Click on a free spot inside the puzzle view to add a new internal module.</source>
        <translation>Klicken Sie auf eine freie Stelle im Puzzle, um ein neues internes Modul hinzuzufügen.</translation>
    </message>
    <message>
        <location filename="../main.py" line="1435"/>
        <source>Click on a free spot inside the puzzle view to add a new external module.</source>
        <translation>Klicken Sie auf eine freie Stelle im Puzzle, um ein neues externes Modul hinzuzufügen.</translation>
    </message>
    <message>
        <location filename="../main.py" line="1461"/>
        <source>Click on a free spot inside the puzzle view to add a new pipe.</source>
        <translation>Klicken Sie auf eine freie Stelle im Puzzle, um eine neue Leitung hinzuzufügen.</translation>
    </message>
    <message>
        <location filename="../main.py" line="1474"/>
        <source>Click on a free spot inside the puzzle view to add a new valve.</source>
        <translation>Klicken Sie auf eine freie Stelle im Puzzle, um einen neuen Verteiler hinzuzufügen.</translation>
    </message>
    <message>
        <location filename="../main.py" line="1517"/>
        <source>puzzle locked</source>
        <translation>Puzzle gesperrt</translation>
    </message>
    <message>
        <location filename="../main.py" line="1521"/>
        <source>puzzle unlocked</source>
        <translation>Puzzle entsperrt</translation>
    </message>
    <message>
        <location filename="../main.py" line="1602"/>
        <source>Add lib folder</source>
        <translation>Bibliotheksordner hinzufügen</translation>
    </message>
    <message>
        <location filename="../main.py" line="286"/>
        <source>Welcome to Puzzlestream!
Please create a new project folder using File -&gt; New Project
or open an existing project.</source>
        <translation type="obsolete">Willkommen bei Puzzlestream!
Bitte erstellen Sie einen neuen Projektordner über Datei-&gt;Neues Projekt
oder öffnen Sie ein vorhandenes Projekt.</translation>
    </message>
    <message>
        <location filename="../main.py" line="294"/>
        <source>Add a new module or select an existing one to edit its source code.</source>
        <translation type="obsolete">Fügen Sie ein Modul hinzu oder wählen Sie ein bereits vorhandenes
aus, um seinen Code zu bearbeiten.</translation>
    </message>
    <message>
        <location filename="../main.py" line="274"/>
        <source>Click left on the scrollable puzzle region to add a </source>
        <translation>Klicken Sie links auf eine freie Stelle im Puzzle, um ein</translation>
    </message>
    <message>
        <location filename="../main.py" line="445"/>
        <source>Open project</source>
        <translation>Projekt öffnen</translation>
    </message>
    <message>
        <location filename="../main.py" line="453"/>
        <source>Save file</source>
        <translation>Datei speichern</translation>
    </message>
    <message>
        <location filename="../main.py" line="454"/>
        <source>Back</source>
        <translation>Rückgängig</translation>
    </message>
    <message>
        <location filename="../main.py" line="455"/>
        <source>Forward</source>
        <translation>Wiederholen</translation>
    </message>
    <message>
        <location filename="../main.py" line="456"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../main.py" line="457"/>
        <source>Cut</source>
        <translation>Ausschneiden</translation>
    </message>
    <message>
        <location filename="../main.py" line="458"/>
        <source>Paste</source>
        <translation>Einfügen</translation>
    </message>
    <message>
        <location filename="../main.py" line="463"/>
        <source>Run puzzle</source>
        <translation>Durchlauf des Puzzles starten</translation>
    </message>
    <message>
        <location filename="../main.py" line="464"/>
        <source>Pause puzzle</source>
        <translation>Durchlauf des Puzzles pausieren</translation>
    </message>
    <message>
        <location filename="../main.py" line="465"/>
        <source>Stop puzzle</source>
        <translation>Durchlauf des Puzzles stoppen</translation>
    </message>
    <message>
        <location filename="../main.py" line="466"/>
        <source>Puzzle</source>
        <translation>Puzzle</translation>
    </message>
    <message>
        <location filename="../main.py" line="467"/>
        <source>Data view</source>
        <translation>Datenanzeige</translation>
    </message>
    <message>
        <location filename="../main.py" line="468"/>
        <source>Plot view</source>
        <translation>Plotanzeige</translation>
    </message>
    <message>
        <location filename="../main.py" line="396"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../main.py" line="459"/>
        <source>&amp;Format code</source>
        <translation>&amp;Code formatieren</translation>
    </message>
    <message>
        <location filename="../main.py" line="461"/>
        <source>&amp;Sort imports</source>
        <translation>&amp;Importe sortieren</translation>
    </message>
    <message>
        <location filename="../main.py" line="951"/>
        <source>pip package manager</source>
        <translation>pip Paketmanager</translation>
    </message>
    <message>
        <location filename="../main.py" line="1649"/>
        <source>get-puzzlestream/</source>
        <translation>de/herunterladen/</translation>
    </message>
    <message>
        <location filename="../main.py" line="1660"/>
        <source>An update to version </source>
        <translation>Ein Update auf Version </translation>
    </message>
    <message>
        <location filename="../main.py" line="1660"/>
        <source> is available; you are currently using version </source>
        <translation> ist verfügbar; Sie benutzen zur Zeit Version </translation>
    </message>
    <message>
        <location filename="../main.py" line="1660"/>
        <source>Please click &lt;a href=&quot;https://puzzlestream.org/</source>
        <translation>Bitte klicken Sie &lt;a href=&quot;https://puzzlestream.org/</translation>
    </message>
    <message>
        <location filename="../main.py" line="1660"/>
        <source>&quot;&gt;here&lt;/a&gt; for update instructions.</source>
        <translation>&quot;&gt;hier&lt;/a&gt; für eine Updateanleitung.</translation>
    </message>
    <message>
        <location filename="../main.py" line="271"/>
        <source>&lt;img src=&quot;:/Puzzlestream.png&quot; width=&quot;100&quot; height=&quot;100&quot;&gt;&lt;p&gt;You may &lt;a href=&quot;#new_project&quot;&gt;create a new project folder&lt;/a&gt; or &lt;a href=&quot;#open_project&quot;&gt;open an existing project&lt;/a&gt;&lt;br&gt;and start working.&lt;/p&gt;&lt;br&gt;&lt;p&gt;&lt;font size=&quot;-1&quot;&gt;Last projects:&lt;/font&gt;&lt;/p&gt;</source>
        <translation type="obsolete">&lt;img src=&quot;:/Puzzlestream.png&quot; width=&quot;100&quot; height=&quot;100&quot;&gt;&lt;p&gt;Sie können &lt;a href=&quot;#new_project&quot;&gt;einen neuen Projektordner erstellen&lt;/a&gt; oder &lt;a href=&quot;#open_project&quot;&gt;einen vorhandenen öffnen&lt;/a&gt;&lt;br&gt;und mit der Arbeit beginnen.&lt;/p&gt;&lt;br&gt;&lt;p&gt;&lt;font size=&quot;-1&quot;&gt;Zuletzt verwendete Projekte:&lt;/font&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../main.py" line="266"/>
        <source>Add a new module or select an existing one&lt;br&gt;to edit its source code.</source>
        <translation>Fügen Sie ein neues Modul hinzu oder wählen Sie ein vorhandenes aus,&lt;br&gt;um den entsprechenden Code zu bearbeiten.</translation>
    </message>
    <message>
        <location filename="../main.py" line="449"/>
        <source>&amp;Recent projects</source>
        <translation>&amp;Zuletzt verwendet</translation>
    </message>
    <message>
        <location filename="../main.py" line="451"/>
        <source>&amp;Close project</source>
        <translation>Projekt &amp;schließen</translation>
    </message>
    <message>
        <location filename="../main.py" line="443"/>
        <source>New project</source>
        <translation>Neues Projekt</translation>
    </message>
    <message>
        <location filename="../main.py" line="1356"/>
        <source>Loading project...</source>
        <translation>Lade Projekt...</translation>
    </message>
    <message>
        <location filename="../main.py" line="419"/>
        <source>&amp;Save debug information</source>
        <translation>&amp;Debug-Informationen speichern</translation>
    </message>
    <message>
        <location filename="../main.py" line="1112"/>
        <source>Save debug information</source>
        <translation>Debug-Informationen speichern</translation>
    </message>
    <message>
        <location filename="../main.py" line="1112"/>
        <source>Zip files (*.zip)</source>
        <translation>Zip Dateien (*zip)</translation>
    </message>
    <message>
        <location filename="../main.py" line="1143"/>
        <source>The debug information was successfully saved to</source>
        <translation>Die Debug-Informationen wurden erfolgreich gespeichert unter</translation>
    </message>
    <message>
        <location filename="../main.py" line="1150"/>
        <source>An error occured while saving the debug information:</source>
        <translation>Ein Fehler trat beim Speichern der Debug-Informationen auf:</translation>
    </message>
    <message>
        <location filename="../main.py" line="1391"/>
        <source>Save project folder</source>
        <translation>Projekt-Ordner speichern</translation>
    </message>
    <message>
        <location filename="../main.py" line="413"/>
        <source>&amp;User guide</source>
        <translation>&amp;Anleitung</translation>
    </message>
    <message>
        <location filename="../main.py" line="417"/>
        <source>&amp;Puzzlestream website</source>
        <translation>&amp;Puzzlestream Website</translation>
    </message>
    <message>
        <location filename="../main.py" line="247"/>
        <source>&lt;img src=&quot;:/Puzzlestream.png&quot; width=&quot;128&quot; height=&quot;128&quot;&gt;&lt;p&gt;You may &lt;a href=&quot;#new_project&quot;&gt;&lt;span style=&quot;color: #177dc9;&quot;&gt;create a new project folder&lt;/span&gt;&lt;/a&gt; or &lt;a href=&quot;#open_project&quot;&gt;&lt;span style=&quot;color: #177dc9;&quot;&gt;open an existing project&lt;/span&gt;&lt;/a&gt;&lt;br&gt;and start working.&lt;/p&gt;&lt;br&gt;&lt;p&gt;&lt;font size=&quot;-1&quot;&gt;Last projects:&lt;/font&gt;&lt;/p&gt;</source>
        <translation>&lt;img src=&quot;:/Puzzlestream.png&quot; width=&quot;128&quot; height=&quot;128&quot;&gt;&lt;p&gt;Sie können &lt;a href=&quot;#new_project&quot;&gt;&lt;span style=&quot;color: #177dc9;&quot;&gt;einen neuen Projektordner erstellen&lt;/span&gt;&lt;/a&gt; oder &lt;a href=&quot;#open_project&quot;&gt;&lt;span style=&quot;color: #177dc9;&quot;&gt;einen Ordner öffnen&lt;/span&gt;&lt;/a&gt;&lt;br&gt;und mit der Arbeit beginnen.&lt;/p&gt;&lt;br&gt;&lt;p&gt;&lt;font size=&quot;-1&quot;&gt;Zuletzt geöffnete Projekte:&lt;/font&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../main.py" line="384"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../main.py" line="385"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../main.py" line="387"/>
        <source>Apps</source>
        <translation>Apps</translation>
    </message>
    <message>
        <location filename="../main.py" line="388"/>
        <source>Libraries</source>
        <translation>Bibliotheken</translation>
    </message>
    <message>
        <location filename="../main.py" line="389"/>
        <source>Stream</source>
        <translation>Strom</translation>
    </message>
    <message>
        <location filename="../main.py" line="390"/>
        <source>Git</source>
        <translation>Git</translation>
    </message>
    <message>
        <location filename="../main.py" line="391"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../main.py" line="772"/>
        <source>From template</source>
        <translation>Aus Vorlage</translation>
    </message>
    <message>
        <location filename="../main.py" line="1209"/>
        <source>-&gt; Manage templates</source>
        <translation>-&gt; Vorlagen verwalten</translation>
    </message>
    <message>
        <location filename="../main.py" line="1070"/>
        <source>You have to choose a module before starting an app.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Manager</name>
    <message>
        <location filename="../manager.py" line="519"/>
        <source>You cannot move a selection that is not one connected block.</source>
        <translation>Sie können keine Auswahl verschieben, die nicht aus einem einzigen, verbundenen Block besteht.</translation>
    </message>
    <message>
        <location filename="../manager.py" line="599"/>
        <source>Module_</source>
        <translation>Modul_</translation>
    </message>
    <message>
        <location filename="../manager.py" line="696"/>
        <source>Project locked</source>
        <translation>Projekt gesperrt</translation>
    </message>
    <message>
        <location filename="../manager.py" line="695"/>
        <source>The project you are trying to open is locked. Please close the Puzzlestream instance using this project folder first.

If this message is due to a crash of Puzzlestream the last time you used that project, please delete the &apos;.lock&apos; file in the project folder in order to be able to open the project again.</source>
        <translation type="obsolete">Das Projekt, das Sie gerade öffnen möchten, ist gesperrt. Bitte schließen Sie zuerst die Puzzlestream-Instanz, die zur Zeit auf den Projektordner zugreift.

Falls diese Nachricht durch einen Crash von Puzzlestream bei der letzten Benutzung des Projekts verursacht wird, löschen Sie bitte die &apos;.lock&apos;-Datei im Projektordner, um das Projekt wieder zu benutzen.</translation>
    </message>
    <message>
        <location filename="../manager.py" line="697"/>
        <source>The project you were trying to open is located here:
</source>
        <translation>Das Projekt, das Sie öffnen wollten, befindet sich hier:
</translation>
    </message>
    <message>
        <location filename="../manager.py" line="88"/>
        <source>Git is not installed. Please install the git version control system as described &lt;a href=&quot;https://git-scm.com/book/en/v2/Getting-Started-Installing-Git&quot;&gt;here&lt;/a&gt;.</source>
        <translation>Git ist nicht installiert. Bitte installieren Sie das Git Versionsverwaltungssystem wie &lt;a href=&quot;https://git-scm.com/book/en/v2/Getting-Started-Installing-Git&quot;&gt;hier&lt;/a&gt; beschrieben.</translation>
    </message>
    <message>
        <location filename="../manager.py" line="697"/>
        <source>The project you are trying to open is locked. Please close the Puzzlestream instance using this project folder first.

If you are sure that nobody else is currently accessing the project, you may unlock it using the button below.</source>
        <translation>Das Projekt, das Sie gerade öffnen möchten, ist gesperrt. Bitte schließen Sie zuerst die Puzzlestream-Instanz, die zur Zeit auf den Projektordner zugreift.
Wenn Sie sicher sind, dass niemand sonst dieses Projekt zur Zeit benutzt, können Sie es mit dem Knopf unten entsperren.</translation>
    </message>
    <message>
        <location filename="../manager.py" line="710"/>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../manager.py" line="714"/>
        <source>Unlock project</source>
        <translation>Projekt entsperren</translation>
    </message>
    <message>
        <location filename="../manager.py" line="762"/>
        <source>Git error:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Module</name>
    <message>
        <location filename="../module.py" line="365"/>
        <source>Run time</source>
        <translation>Dauer des letzten Durchlaufs</translation>
    </message>
    <message>
        <location filename="../module.py" line="367"/>
        <source>Save time</source>
        <translation>Dauer des letzten Speicherns</translation>
    </message>
    <message>
        <location filename="../module.py" line="371"/>
        <source>Test report</source>
        <translation>Testbericht</translation>
    </message>
    <message>
        <location filename="../module.py" line="380"/>
        <source>FAILED</source>
        <translation>FEHLGESCHLAGEN</translation>
    </message>
    <message>
        <location filename="../module.py" line="377"/>
        <source>SUCCESSFUL</source>
        <translation>ERFOLGREICH</translation>
    </message>
    <message>
        <location filename="../module.py" line="362"/>
        <source>Last run</source>
        <translation>Letzter Durchlauf</translation>
    </message>
    <message>
        <location filename="../module.py" line="284"/>
        <source>A file with this name already exists. Please choose another name or delete the existing file.</source>
        <translation>Eine Datei mit diesem Namen existiert bereits. Bitte wählen Sie einen anderen Namen oder löschen Sie die vorhandene Datei.</translation>
    </message>
</context>
<context>
    <name>NotificationTab</name>
    <message>
        <location filename="../notificationtab.py" line="45"/>
        <source>Delete all</source>
        <translation>Alle löschen</translation>
    </message>
</context>
<context>
    <name>PipGui</name>
    <message>
        <location filename="../pip.py" line="240"/>
        <source>Available</source>
        <translation>Verfügbar</translation>
    </message>
    <message>
        <location filename="../pip.py" line="137"/>
        <source>Installed</source>
        <translation>Installiert</translation>
    </message>
    <message>
        <location filename="../pip.py" line="138"/>
        <source>Updates</source>
        <translation>Aktualisierungen</translation>
    </message>
    <message>
        <location filename="../pip.py" line="139"/>
        <source>install -&gt;</source>
        <translation>installieren -&gt;</translation>
    </message>
    <message>
        <location filename="../pip.py" line="140"/>
        <source>&lt;- uninstall</source>
        <translation>&lt;- deinstallieren</translation>
    </message>
    <message>
        <location filename="../pip.py" line="141"/>
        <source>&lt;- update</source>
        <translation>&lt;- aktualisieren</translation>
    </message>
</context>
<context>
    <name>PlotView</name>
    <message>
        <location filename="../plotview.py" line="48"/>
        <source>Plot view</source>
        <translation>Plotanzeige</translation>
    </message>
    <message>
        <location filename="../plotview.py" line="45"/>
        <source>complete stream</source>
        <translation>kompletter Strom</translation>
    </message>
    <message>
        <location filename="../plotview.py" line="51"/>
        <source>Add viewer</source>
        <translation>Anzeigefenster hinzufügen</translation>
    </message>
    <message>
        <location filename="../plotview.py" line="52"/>
        <source>Tile windows</source>
        <translation>Fenster anordnen</translation>
    </message>
    <message>
        <location filename="../plotview.py" line="53"/>
        <source>auto tile</source>
        <translation>automatisch anordnen</translation>
    </message>
</context>
<context>
    <name>Preferences</name>
    <message>
        <location filename="../preferences.py" line="50"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="../preferences.py" line="51"/>
        <source>Appearance</source>
        <translation>Aussehen</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="11"/>
        <source>Number of concurrent processes
(0 = number of CPU cores, recommended)</source>
        <translation>Anzahl der simultanen Prozesse
(0 = Anzahl der CPU-Kerne, empfohlen)</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="13"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="14"/>
        <source>Show clock only when in full screen</source>
        <translation>Uhr im Vollbildmodus anzeigen</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="15"/>
        <source>Save before running modules</source>
        <translation>Vor dem Ausführen von Modulen speichern</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="16"/>
        <source>Save when editor loses focus</source>
        <translation>Speichern, wenn der Editor den Fokus verliert</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="20"/>
        <source>Design</source>
        <translation>Aussehen</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="21"/>
        <source>dark</source>
        <translation>dunkel</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="22"/>
        <source>light</source>
        <translation>hell</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="17"/>
        <source>Slower drawing (activate if the puzzle view is not redrawn correctly)</source>
        <translation type="obsolete">Langsameres Zeichnen (aktivieren, falls Sie Probleme beim Zeichnen des Puzzles beobachten)</translation>
    </message>
    <message>
        <location filename="../preferencestranslation.py" line="17"/>
        <source>Slower drawing (activate if the
puzzle view is not redrawn correctly)</source>
        <translation>Langsameres Zeichnen (aktivieren, falls Sie
Probleme beim Zeichnen des Puzzles beobachten)</translation>
    </message>
</context>
<context>
    <name>SinglePlotView</name>
    <message>
        <location filename="../plotview.py" line="191"/>
        <source>previous</source>
        <translation>zurück</translation>
    </message>
    <message>
        <location filename="../plotview.py" line="192"/>
        <source>next</source>
        <translation>weiter</translation>
    </message>
</context>
<context>
    <name>Status</name>
    <message>
        <location filename="../puzzleitem.py" line="66"/>
        <source>incomplete</source>
        <translation>unvollständig</translation>
    </message>
    <message>
        <location filename="../puzzleitem.py" line="67"/>
        <source>error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../puzzleitem.py" line="68"/>
        <source>running</source>
        <translation>läuft</translation>
    </message>
    <message>
        <location filename="../puzzleitem.py" line="69"/>
        <source>test failed</source>
        <translation>Test fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../puzzleitem.py" line="70"/>
        <source>paused</source>
        <translation>pausiert</translation>
    </message>
    <message>
        <location filename="../puzzleitem.py" line="71"/>
        <source>waiting</source>
        <translation>wartet</translation>
    </message>
    <message>
        <location filename="../puzzleitem.py" line="72"/>
        <source>finished</source>
        <translation>fertig</translation>
    </message>
</context>
<context>
    <name>TablePlot</name>
    <message>
        <location filename="../tableplot.py" line="72"/>
        <source>curve</source>
        <translation>Kurve</translation>
    </message>
    <message>
        <location filename="../tableplot.py" line="73"/>
        <source>histogram</source>
        <translation>Histogramm</translation>
    </message>
</context>
<context>
    <name>Traceback</name>
    <message>
        <location filename="../tracebackview.py" line="32"/>
        <source>Traceback</source>
        <translation>Rückverfolgung</translation>
    </message>
</context>
<context>
    <name>TreeModel</name>
    <message>
        <location filename="../../backend/treemodel.py" line="41"/>
        <source>1D items</source>
        <translation>1D Elemente</translation>
    </message>
    <message>
        <location filename="../../backend/treemodel.py" line="42"/>
        <source>2D items</source>
        <translation>2D Elemente</translation>
    </message>
    <message>
        <location filename="../../backend/treemodel.py" line="45"/>
        <source>Plot items</source>
        <translation>Plot Elemente</translation>
    </message>
    <message>
        <location filename="../../backend/treemodel.py" line="46"/>
        <source>Other items</source>
        <translation>Sonstige Elemente</translation>
    </message>
    <message>
        <location filename="../../backend/treemodel.py" line="43"/>
        <source>Pandas data frames</source>
        <translation>Pandas Data Frames</translation>
    </message>
</context>
</TS>
