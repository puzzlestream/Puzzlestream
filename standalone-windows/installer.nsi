;---------------------------------
;General
!include MUI2.nsh

# define name of installer
OutFile "build/Puzzlestream-inst.exe"
 
# For removing Start Menu shortcut in Windows 7
RequestExecutionLevel user
 
;--------------------------------
;Folder selection page
!define MUI_PRODUCT "Puzzlestream"
InstallDir "$PROFILE\AppData\Local\Programs\${MUI_PRODUCT}"

# MUI configuration
!define MUI_COMPONENTSPAGE_SMALLDESC ;No value
!define MUI_INSTFILESPAGE_COLORS "323232 FFFFFF" ;Two colors
!define MUI_ICON ".\python-windows\Lib\site-packages\puzzlestream\icons\Puzzlestream.ico"
!define MUI_UNICON ".\python-windows\Lib\site-packages\puzzlestream\icons\Puzzlestream.ico"
!define MUI_WELCOMEPAGE_TITLE "Puzzlestream installer"
!define MUI_WELCOMEPAGE_TEXT "Puzzlestream is an interactive analysis enviroment for Python, providing a fast and simple way from raw data to meaningful results and visualisations. By organising your code in modules, Puzzlestream gives you an instantaneous overview of your project's structure - however complicated it may be. Highly interactive graphical interfaces support you in gaining an intuition for your data, asking the right questions and finding the corresponding answers.$\r$\n$\r$\nPlease click $\"next$\" to proceed with the installation."
!define MUI_LICENSEPAGE_TEXT_TOP "Please agree to the Puzzlestream license (MIT):"

!define MUI_FINISHPAGE_SHOWREADME ""
!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
!define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
!define MUI_FINISHPAGE_SHOWREADME_FUNCTION finishpageaction

Name "Puzzlestream"
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "..\LICENSE.txt"
; !insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

# start default section
Section
    # copy files
    SetOutPath "$INSTDIR\python-windows\"
    File /nonfatal /a /r "python-windows\"

    # set the installation directory as the destination for the following actions
    SetOutPath $INSTDIR

    # copy main .exe and main .py
    File "Puzzlestream.exe"
 
    # create the uninstaller
    WriteUninstaller "$INSTDIR\uninstall.exe"

    # create start menu folder
    CreateDirectory "$SMPrograms\Puzzlestream"

    # start menu entry for uninstaller
    CreateShortCut "$SMPROGRAMS\Puzzlestream\Uninstall.lnk" "$INSTDIR\uninstall.exe"

    # start menu entry for software
    CreateShortCut "$SMPROGRAMS\Puzzlestream\Puzzlestream.lnk" "$INSTDIR\Puzzlestream.exe"

    # register
    WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\Puzzlestream" \
                     "DisplayName" "Puzzlestream"
    WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\Puzzlestream" \
                    "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
SectionEnd
 
# uninstaller section start
Section "uninstall"
    # delete the installation directory
    RMDIR /r $INSTDIR
 
    # remove the link from the start menu
    Delete "$SMPROGRAMS\Puzzlestream\Puzzlestream.lnk"
    Delete "$SMPROGRAMS\Puzzlestream\Uninstall.lnk"

    # remove link from desktop
    Delete "$DESKTOP\Puzzlestream.lnk"

    # unregister
    DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\Puzzlestream"
# uninstaller section end
SectionEnd

Function finishpageaction
CreateShortcut "$DESKTOP\Puzzlestream.lnk" "$INSTDIR\Puzzlestream.exe"
FunctionEnd

Function .onInit
 
  ReadRegStr $R0 HKCU \
  "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PROGRAM_NAME}" \
  "UninstallString"
  StrCmp $R0 "" done
 
  MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION \
  "${PROGRAM_NAME} is already installed. $\n$\nClick `OK` to remove the \
  previous version or `Cancel` to cancel this upgrade." \
  IDOK uninst
  Abort
 
;Run the uninstaller
uninst:
  ClearErrors
  ExecWait '$R0 _?=$INSTDIR' ;Do not copy the uninstaller to a temp file
 
  IfErrors no_remove_uninstaller done
    ;You can either use Delete /REBOOTOK in the uninstaller or add some code
    ;here to remove the uninstaller. Use a registry key to check
    ;whether the user has chosen to uninstall. If you are using an uninstaller
    ;components page, make sure all sections are uninstalled.
  no_remove_uninstaller:
 
done:
 
FunctionEnd